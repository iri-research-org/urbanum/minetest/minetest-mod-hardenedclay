# minetest-mod-hardenedclay

This mod is extacted from https://github.com/ignfab-minalac/minalac/tree/master/sources/resources/map-mods/hardenedclay to support "Minecraft à la carte" worlds.

Copyright (c) Institut national de l'information géographique et forestière

This program and the accompanying materials are made available under the terms of the GPL License, Version 3.0.